class Temperature
  def initialize(hash)
    @f = hash[:f]
    @c = hash[:c]
  end

  def self.from_celsius(num)
    self.new(:c => num)
  end

  def self.from_fahrenheit(num)
    self.new(:f => num)
  end

  def in_fahrenheit()
    @c.nil? ? @f : (@c * 9 / 5.0) + 32
  end

  def in_celsius()
    @f.nil? ? @c : (@f - 32) * 5 / 9.0
  end
end

class Celsius < Temperature
  def initialize(num)
    super(c: num)
  end
end

class Fahrenheit < Temperature
  def initialize(num)
    super(f: num)
  end
end
