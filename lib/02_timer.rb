class Timer
  attr_accessor :seconds

  def initialize()
    @seconds = 0
  end

  def seconds()
    @seconds
  end

  def time_string
    hours = (@seconds / 3600).to_s
    hours = "0" + hours if hours.length == 1
    minutes = ((@seconds - hours.to_i * 3600) / 60).to_s
    minutes = "0" + minutes if minutes.length == 1
    seconds = (@seconds % 60).to_s
    seconds = "0" + seconds if seconds.length == 1
    "#{hours}:#{minutes}:#{seconds}"
  end
end
