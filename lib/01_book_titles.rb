class Book
  def title=(str)
    @book = str
  end

  def title()
    exceptions = ["and", "in", "of", "the", "a", "an"]
    @book = @book.split(" ").map{|str| exceptions.include?(str) ? str : str.capitalize}
    @book[0].capitalize!
    @book.join(" ")
  end
end
