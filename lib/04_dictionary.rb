class Dictionary
  def initialize(hash = {})
    @d = hash
  end

  def entries()
    @d
  end

  def add(input)
    if input.is_a?(String)
      @d.merge!(input => nil)
    else
      @d.merge!(input)
    end
  end

  def keywords()
    @d.keys.sort
  end

  def include?(key)
    @d.keys.include?(key)
  end

  def find(prefix)
    @d.select{|key, val| key.start_with?(prefix)}
  end

  def printable
    printable_arr = []
    @d.sort.each do |key, val|
      printable_arr << "[#{key}] \"#{val}\""
    end
    printable_arr.join("\n")
  end
end
